// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include "benchmark.hpp"

#include <functional>
#include <iostream>
#include <memory>
#include <thread>
#include <tuple>

#include "contra/test_utilities/test_data.hpp"

#ifdef CONTRA_SHARED_MEMORY_AVAILABLE
#include "contra/shared_memory/shared_memory_transport.hpp"

void SharedMemoryBenchmark(benchmark::State& state) {
  const int64_t iteration_count = state.range(0);

  contra::SharedMemoryTransport server;
  contra::SharedMemoryTransport client;
  contra::Packet packet = test_utilities::ANY_PACKET;

  for (auto _ : state) {
    int64_t received = 0;
    for (int64_t i = 0; i < iteration_count; ++i) {
      server.Send(packet, true);
      received += client.Receive(0).size();
    }
    while (received < iteration_count) {
      received += client.Receive(0).size();
    }
  }
}

BENCHMARK(SharedMemoryBenchmark)
    ->Arg(1)
    ->Arg(10)
    ->Arg(100)
    ->Arg(1000)
    ->Unit(benchmark::kMillisecond);
#endif

#ifdef CONTRA_ZMQ_AVAILABLE
#include "contra/zmq/zeromq_transport.hpp"

void ZMQBenchmark(benchmark::State& state) {
  const int64_t iteration_count = state.range(0);

  contra::ZMQTransport server(static_cast<uint16_t>(5555));
  contra::ZMQTransport client("localhost:5555");
  contra::Packet packet = test_utilities::ANY_PACKET;

  for (auto _ : state) {
    int64_t received = 0;
    for (int64_t i = 0; i < iteration_count; ++i) {
      server.Send(packet, true);
      received += client.Receive(0).size();
    }
    while (received < iteration_count) {
      received += client.Receive(0).size();
    }
  }
}

BENCHMARK(ZMQBenchmark)
    ->Arg(1)
    ->Arg(10)
    ->Arg(100)
    ->Arg(1000)
    ->Unit(benchmark::kMillisecond);
#endif

#ifdef CONTRA_WEBSOCKETS_AVAILABLE
#include "contra/websockets/bwebsockets_transport_client.hpp"
#include "contra/websockets/bwebsockets_transport_server.hpp"

void WebSocketBenchmark(benchmark::State& state) {
  const int64_t iteration_count = state.range(0);

  contra::BWSTServer server(5555);
  contra::BWSTClient client("127.0.0.1:5555", 30);
  contra::Packet packet = test_utilities::ANY_PACKET;

  for (auto _ : state) {
    int64_t received = 0;
    for (int64_t i = 0; i < iteration_count; ++i) {
      server.Send(packet, true);
      received += client.Receive(0).size();
    }
    while (received < iteration_count) {
      received += client.Receive(0).size();
    }
  }
}

BENCHMARK(WebSocketBenchmark)
    ->Arg(1)
    ->Arg(10)
    ->Arg(100)
    ->Arg(1000)
    ->Unit(benchmark::kMillisecond);
#endif

BENCHMARK_MAIN();
