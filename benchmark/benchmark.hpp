#ifndef CONTRA_BENCHMARK_BENCHMARK_HPP
#define CONTRA_BENCHMARK_BENCHMARK_HPP

#include <contra/bench/suppress_warnings.hpp>

SUPPRESS_WARNINGS_BEGIN
#include <benchmark/benchmark.h>
SUPPRESS_WARNINGS_END

void SharedMemoryBenchmark(benchmark::State& state);
void ZMQBenchmark(benchmark::State& state);
void WebSocketBenchmark(benchmark::State& state);

#endif
