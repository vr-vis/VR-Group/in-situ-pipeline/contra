# contra -- a lightweight transport library for conduit data

[License](https://devhub.vr.rwth-aachen.de/VR-Group/contra/blob/master/LICENSE)

[Latest Version](https://devhub.vr.rwth-aachen.de/VR-Group/contra)

![Build Status](https://devhub.vr.rwth-aachen.de/VR-Group/contra/badges/master/build.svg)

# Introduction

Contra is designed to transport conduit data between processes.
For doing so it offers different kinds of transport layers which can be used.

Contra has been cross-developed for both Windows and Unix systems.

## Supported compilers
- msvc (tested with msvc++14.1) 
- gcc (tested with gcc 5.3.1, 6.3.1 and 7.3.1)
- clang (tested with clang 9.1 and 10.0)
  
Other compilers versions might work as well. 

## Transport protocols
Contra can transport the conduit data using a variety of different transport protocols.

- File Transport
    - This is the basic transport method which comes with contra mainly for testing purposes. 

- Shared Memory Transport
  - This library uses Boost.Interproces to enable IPC via shared memory.

- ZeroMQ Transport
    - A TCP based transport relying on ZeroMQ for the Network Communication.

# Getting contra

## Requirements
The version numbers specify the versions used for testing. Other versions may also work.

### Required
  - [CMake](https://cmake.org/) (v.3.6.0 or higher)
  - [Conduit](https://github.com/LLNL/conduit) (0.3.1)

### Optional
When you want the python bindings (`-DWITH_PYTHON_BINDINGS=ON`, `OFF` by default) you will also need the following:
  - [Boost.Python](https://www.boost.org/users/download/)
  - [Python](https://www.python.org/)

If you want to to contribute to the library you want to have the developer tests enabled (`-DENABLE_DEVELOPER_TESTS=ON`, `OFF` by default) and you will also need:
  - [Cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint)
  - [Cppcheck](https://sourceforge.net/projects/cppcheck/)

When building with Shared Memory Transport you will also need:
  - [Boost.Interprocess](https://www.boost.org/users/download/)

When building with ZeroMQ Transport you will also need:
  - [cppzmq](https://github.com/zeromq/cppzmq) which also requires
  - [ZeroMQ](https://github.com/zeromq/cppzmq)

## Building contra

1. Install all required dependencies (see above).
2. Create a build directory and run CMake from there `cmake [FLAGS] -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY] [PATH_TO_CONTRA_SOURCE]`. Where `[INSTALL_DIRECTORY]` is the directory where the library should be installed and `[PATH_TO_CONTRA_SOURCE]` is the path to the root of this repository. The values for `[FLAGS]` depends on your configuration (see below).
3. Run `make [-j]` to build the library.
4. Run `make test` to check if everything went fine (only if `ENABLE_TESTS` was set to `ON` default: `ON`).
5. Run `make install` to install the library.

### CMake configurations
#### Minimal Build:
```
cmake
  -DCONDUIT_DIR=[PATH_WHERE_CONDUIT_IS_INSTALLED]
  -DENABLE_TESTS=OFF
  -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY]
  [PATH_TO_CONTRA_SOURCE]
```

#### With tests
```
cmake
  -DCONDUIT_DIR=[PATH_WHERE_CONDUIT_IS_INSTALLED]
  -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY]
  [PATH_TO_CONTRA_SOURCE]
```

#### With python bindings
```
cmake
  -DCONDUIT_DIR=[PATH_WHERE_CONDUIT_IS_INSTALLED]
  -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY]
  -DWITH_PYTHON_BINDINGS=ON
  [PATH_TO_CONTRA_SOURCE]
```
If CMake cannot find boost, can specify the directory where boost is installed via `-DBOOST_ROOT=[PATH_WHERE_BOOST_IS_INSTALLED]`. You can specify the python version via `-DPython_ADDITIONAL_VERSIONS=[PYTHON_VERSION_TO_USE]` or be more explicit by setting `PYTHON_EXECUTABLE` to the path of the python you want to use, e.g., `-DPYTHON_EXECUTABLE=python3.7`. Make sure you also the python libraries and boost-python installed for this version.

#### With shared memory transport
```
cmake
  -DCONDUIT_DIR=[PATH_WHERE_CONDUIT_IS_INSTALLED]
  -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY]
  -DWITH_SHARED_MEMORY=1
  [PATH_TO_CONTRA_SOURCE]
```
If CMake cannot find boost, can specify the directory where boost is installed via `-DBOOST_ROOT=[PATH_WHERE_BOOST_IS_INSTALLED]`.

#### With ZeroMQ transport
```
cmake
  -DCONDUIT_DIR=[PATH_WHERE_CONDUIT_IS_INSTALLED]
  -DCMAKE_INSTALL_PREFIX=[INSTALL_DIRECTORY]
  -DWITH_ZEROMQ=1
  [PATH_TO_CONTRA_SOURCE]
```
If CMake cannot find cppzmq, can specify the directory where boost is installed via `-Dcppzmq_DIR=[PATH_WHERE_CPPZMQ_IS_INSTALLED]`.

## Using Contra

```
find_package(contra REQUIRED core zmq shared_memory)
# ...
target_link_libraries(... contra_core contra_zmq contra_shared_memory)
```
See `test_package/CMakeLists.txt` for an example.

Contra is designed to use Relays as communication nodes.
In most cases you will have 2 relays Communicating which each other. For the ZeroMQ transport you can have multiple CLIENT relays though.
A relay uses a Transport layer defined at construction e.g:
```cpp
contra::Relay<contra::SharedMemoryTransport> relay;
```

#### The constructor arguments for the different layers with examples:
- File Transport:
    - Filenename (String) The names of the two relays communicating have to match!

```cpp
contra::Relay<contra::FileTransport> relay("testfile.contra");
```
- Shared Memory Transport:
    - Memory name(String) (Default = "contraShMemTransp")The names of the two relays communicating have to match!

```cpp
contra::Relay<contra::SharedMemoryTransport> relay;
```
- ZeroMQ Transport:
    - Type (SERVER/CLIENT)
    - TCP Address (String)
    - Optional flag to indicate if you want the relay to wait for a connection to be established before sending Data (boolean)

```cpp
contra::Relay<contra::ZMQTransport> relay(contra::Relay<contra::ZMQTransport>(contra::ZMQTransport::Type::SERVER, "tcp://*:5555");
```


#### Sample sending relay:
```cpp
#include <string>

#include "contra/boost-shmem/shared_memory_transport.hpp"
#include "contra/relay.hpp"

int main() {
  conduit::Node n;
  n["foo/bar"] = 3.14;
  contra::Relay<contra::SharedMemoryTransport> relay;
  for (int i = 0; i < 5; ++i) {
    relay.Send(n);
  }
  return EXIT_SUCCESS;
}
```
#### Sample recieving relay:
```cpp
#include <string>
#include <thread>
#include "contra/boost-shmem/shared_memory_transport.hpp"
#include "contra/relay.hpp"

int main() {
  contra::Relay<contra::SharedMemoryTransport> relay;
  std::vector<conduit::Node> data;
  do {
    data = relay.Receive();
    std::this_thread::sleep_for(std::chrono::seconds(1));
  } while (data.size() == 0);

  std::cout << data[0]["foo/bar"].to_double();

  return EXIT_SUCCESS;
}
```
