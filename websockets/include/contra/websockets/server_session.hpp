// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#ifndef WEBSOCKETS_INCLUDE_CONTRA_WEBSOCKETS_SERVER_SESSION_HPP_
#define WEBSOCKETS_INCLUDE_CONTRA_WEBSOCKETS_SERVER_SESSION_HPP_

#if defined(_WIN32)
#include <SDKDDKVer.h>
#endif

#include <chrono>
#include <condition_variable>
#include <functional>
#include <thread>

#include "contra/packet.hpp"

#include "contra/websockets/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#include "boost/asio/bind_executor.hpp"
#include "boost/asio/buffer.hpp"
#include "boost/asio/ip/tcp.hpp"
#include "boost/asio/strand.hpp"
#include "boost/beast/core.hpp"
#include "boost/beast/websocket.hpp"
SUPPRESS_WARNINGS_END

namespace contra {

namespace websocket = boost::beast::websocket;

SUPPRESS_WARNINGS_BEGIN_PADDED
class ServerSession : public std::enable_shared_from_this<ServerSession> {
  using tcp = boost::asio::ip::tcp;

 public:
  ServerSession(tcp::socket socket, int id,
                std::function<void(int)> delete_callback,
                std::function<void(Packet&)> receive_callback);
  ServerSession(const ServerSession&) = delete;
  ServerSession(ServerSession&&) = default;
  ~ServerSession() = default;
  ServerSession& operator=(const ServerSession&) = delete;
  ServerSession& operator=(ServerSession&&) = delete;

  void Run();
  void Send(const Packet& packet);

  int GetId() const;

 private:
  void Fail(boost::system::error_code ec, char const* what);
  void DoRead();
  void OnAccept(boost::system::error_code ec);
  void OnRead(boost::system::error_code ec, std::size_t bytes_transferred);
  void OnWrite(boost::system::error_code ec, std::size_t bytes_transferred);

  websocket::stream<tcp::socket> ws_;
  boost::asio::strand<boost::asio::io_context::executor_type> strand_;
  boost::beast::multi_buffer send_buffer_;

  bool busy_sending_{false};
  bool ready_{false};
  const int session_id_;
  std::function<void(int)> delete_callback_;
  std::function<void(Packet&)> receive_callback_;

  boost::beast::multi_buffer receive_buffer_;
};
SUPPRESS_WARNINGS_END

}  // namespace contra

#endif  // WEBSOCKETS_INCLUDE_CONTRA_WEBSOCKETS_SERVER_SESSION_HPP_
