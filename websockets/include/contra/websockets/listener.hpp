// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#ifndef WEBSOCKETS_INCLUDE_CONTRA_WEBSOCKETS_LISTENER_HPP_
#define WEBSOCKETS_INCLUDE_CONTRA_WEBSOCKETS_LISTENER_HPP_

#include <functional>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#include "contra/packet.hpp"
#include "contra/websockets/server_session.hpp"

#include "contra/websockets/suppress_warnings.hpp"
SUPPRESS_WARNINGS_BEGIN
#include "boost/asio/bind_executor.hpp"
#include "boost/asio/ip/tcp.hpp"
#include "boost/asio/strand.hpp"
#include "boost/beast/core.hpp"
#include "boost/beast/websocket.hpp"
SUPPRESS_WARNINGS_END

namespace contra {

SUPPRESS_WARNINGS_BEGIN_PADDED
class Listener : public std::enable_shared_from_this<Listener> {
  using tcp = boost::asio::ip::tcp;

 public:
  Listener(boost::asio::io_context* ioc, tcp::endpoint endpoint,
           std::function<void(Packet&)> receive_callback);
  Listener(const Listener&) = delete;
  Listener(Listener&&) = default;
  ~Listener() = default;
  Listener& operator=(const Listener&) = delete;
  Listener& operator=(Listener&&) = delete;

  void Run();
  bool HasOpenSession();
  void Send(const Packet& packet, bool buffer_message_until_client_connects);

 private:
  void DoAccept();
  void OnAccept(boost::system::error_code ec);
  void Fail(boost::system::error_code ec, char const* what);

  tcp::acceptor acceptor_;
  tcp::socket socket_;

  std::vector<std::shared_ptr<ServerSession>> sessions_;
  int next_id_{0};

  std::mutex mutex_;
  std::function<void(Packet&)> receive_callback_;
};
SUPPRESS_WARNINGS_END

}  // namespace contra

#endif  // WEBSOCKETS_INCLUDE_CONTRA_WEBSOCKETS_LISTENER_HPP_
