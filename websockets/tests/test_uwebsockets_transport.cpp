// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include <chrono>
#include <memory>

#include "catch2/catch.hpp"

#include "contra/contra.hpp"
#include "contra/websockets/bwebsockets_transport_client.hpp"
#include "contra/websockets/bwebsockets_transport_server.hpp"

#include "contra/test_utilities/packet_matcher.hpp"
#include "contra/test_utilities/test_data.hpp"

SCENARIO("Server and Client Creation", "[contra][contra::WebsocketTransport]") {
  WHEN("The server is created before the client") {
    contra::BWSTServer server(3000);
    contra::BWSTClient client("127.0.0.1:3000", 30);

    THEN("Server and client are connected") {
      REQUIRE(server.IsConnected() == true);
      REQUIRE(client.IsConnected() == true);
    }

    THEN("Packets can be send and received") {
      server.Send(test_utilities::ANY_PACKET);

      auto received_packets{client.Receive(contra::RECEIVE_WAIT_FOR_PACKET)};
      REQUIRE(received_packets.size() > 0);
    }
  }

  WHEN("The Client is created before the server") {
    contra::BWSTClient client("127.0.0.1:3000", 10);
    contra::BWSTServer server(3000);

    THEN("No connection is established and it doesnt crash") {
      REQUIRE(server.IsConnected() == false);
      REQUIRE(client.IsConnected() == false);
    }
    THEN("Packets sent are not received") {
      server.Send(test_utilities::ANY_PACKET);
      auto received_packets{client.Receive()};
      REQUIRE(received_packets.size() == 0);
    }
  }
}

SCENARIO("Instant destruction of Server and Client ",
         "[contra][contra::ZMQTransport]") {
  GIVEN("a Server and its immediat destruction") {
    contra::BWSTServer server(3000);
  }
  GIVEN("a Client and its immediat destruction") {
    contra::BWSTClient client("127.0.0.1:3000", 5);
  }
}

SCENARIO("Sending and receiving ", "[contra][contra::ZMQTransport]") {
  GIVEN("a Server and a Client") {
    contra::BWSTServer server(3000);
    contra::BWSTClient client("127.0.0.1:3000", 20);

    WHEN("Receive() is called without any packages sent") {
      THEN("received packets are epmty and no exception is thrown") {
        REQUIRE_NOTHROW(client.Receive());
        REQUIRE(client.Receive().size() == 0);
      }
    }

    WHEN("A single package is sent and received") {
      server.Send(test_utilities::ANY_PACKET);
      auto received_packets{client.Receive(contra::RECEIVE_WAIT_FOR_PACKET)};

      THEN("the send package matches the received one") {
        REQUIRE_THAT(received_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
      }
      THEN("only one package is received") {
        REQUIRE(received_packets.size() == 1);
      }
      THEN("calling receive again will deliver 0 packets") {
        auto recieve_again{client.Receive()};
        REQUIRE(recieve_again.empty());
      }
    }

    WHEN("multiple packets are sent") {
      unsigned int factor{1000};

      auto start = std::chrono::high_resolution_clock::now();
      auto now = std::chrono::high_resolution_clock::now();

      for (unsigned int i = 0; i < factor; ++i) {
        server.Send(test_utilities::ANY_PACKET);
        server.Send(test_utilities::ANOTHER_PACKET);
        server.Send(test_utilities::THIRD_PACKET);
      }

      // Track time to check if timeout is reached

      auto received_packets{client.Receive(contra::RECEIVE_WAIT_FOR_PACKET)};

      auto duration =
          std::chrono::duration_cast<std::chrono::milliseconds>(now - start)
              .count();

      while (received_packets.size() < static_cast<uint32_t>(factor * 3) &&
             duration < 700) {
        auto new_packtes{client.Receive(10)};
        received_packets.insert(std::end(received_packets),
                                std::begin(new_packtes), std::end(new_packtes));
        now = std::chrono::high_resolution_clock::now();
        duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(now - start)
                .count();
      }

      std::cout << "Time for " << factor * 3 << " packets: " << duration << "ms"
                << std::endl;

      THEN("the same amount of packages is received within 700 miliseconds") {
        REQUIRE(received_packets.size() == factor * 3);
      }
      THEN("the packets sent match the ones received") {
        REQUIRE_THAT(received_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
        REQUIRE_THAT(received_packets.at(1),
                     Equals(test_utilities::ANOTHER_PACKET));
        REQUIRE_THAT(received_packets.at(999),
                     Equals(test_utilities::ANY_PACKET));
        REQUIRE_THAT(received_packets.at(1000),
                     Equals(test_utilities::ANOTHER_PACKET));
        REQUIRE_THAT(received_packets.at(1001),
                     Equals(test_utilities::THIRD_PACKET));
        REQUIRE_THAT(received_packets.back(),
                     Equals(test_utilities::THIRD_PACKET));
      }
    }
  }
}

SCENARIO("Multiple Clients and one Server ", "[contra][contra::ZMQTransport]") {
  GIVEN("a Server and 3 clients") {
    std::unique_ptr<contra::BWSTServer> server =
        std::make_unique<contra::BWSTServer>(static_cast<uint16_t>(3000));

    std::unique_ptr<contra::BWSTClient> client1 =
        std::make_unique<contra::BWSTClient>("127.0.0.1:3000", 20);
    std::unique_ptr<contra::BWSTClient> client2 =
        std::make_unique<contra::BWSTClient>("127.0.0.1:3000", 20);
    std::unique_ptr<contra::BWSTClient> client3 =
        std::make_unique<contra::BWSTClient>("127.0.0.1:3000", 20);

    THEN("All Connections are established") {
      REQUIRE(server->IsConnected() == true);
      REQUIRE(client1->IsConnected() == true);
      REQUIRE(client2->IsConnected() == true);
      REQUIRE(client2->IsConnected() == true);
    }

    WHEN("A single package is sent") {
      server->Send(test_utilities::ANY_PACKET);

      auto client1_packets{client1->Receive(contra::RECEIVE_WAIT_FOR_PACKET)};
      auto client2_packets{client2->Receive(contra::RECEIVE_WAIT_FOR_PACKET)};
      auto client3_packets{client3->Receive(contra::RECEIVE_WAIT_FOR_PACKET)};

      THEN("the send package matches the received one for all clients") {
        REQUIRE_THAT(client1_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
        REQUIRE_THAT(client2_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
        REQUIRE_THAT(client3_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
      }
    }

    WHEN("all but one Client disconect") {
      client1.reset(nullptr);
      client2.reset(nullptr);

      THEN("The Server still has active connections") {
        REQUIRE(server->IsConnected() == true);
      }
      THEN("The remaining Client still receives packets") {
        server->Send(test_utilities::ANY_PACKET);

        auto client3_packets{client3->Receive(contra::RECEIVE_WAIT_FOR_PACKET)};
        REQUIRE_THAT(client3_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
      }
    }

    WHEN("All Clients disconnect") {
      client1.reset(nullptr);
      client2.reset(nullptr);
      client3.reset(nullptr);
      THEN("The server has no active connection") {
        REQUIRE(server->IsConnected() == false);
      }
    }

    WHEN("The server disconnects  with the clients still running") {
      THEN("the Clients dont throw") { REQUIRE_NOTHROW(server.reset(nullptr)); }
    }
  }
}

SCENARIO("Sendingfrom client to server ", "[contra][contra::ZMQTransport]") {
  GIVEN("a Server and a Client") {
    contra::BWSTServer server(3000);
    contra::BWSTClient client("127.0.0.1:3000", 100);

    WHEN("A single package is sent and received") {
      client.Send(test_utilities::ANY_PACKET);
      auto received_packets{server.Receive(contra::RECEIVE_WAIT_FOR_PACKET)};
      THEN("the send package matches the received one") {
        REQUIRE_THAT(received_packets.front(),
                     Equals(test_utilities::ANY_PACKET));
      }
    }
  }
  GIVEN("a Server and multiple Clients") {
    contra::BWSTServer server(3000);
    contra::BWSTClient client1("127.0.0.1:3000", 100);
    contra::BWSTClient client2("127.0.0.1:3000", 100);
    contra::BWSTClient client3("127.0.0.1:3000", 100);

    WHEN("each client sends a single package") {
      client1.Send(test_utilities::ANY_PACKET);
      client2.Send(test_utilities::ANY_PACKET);
      client3.Send(test_utilities::ANY_PACKET);
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      auto received_packets{server.Receive()};
      THEN("all packets are received") {
        REQUIRE(received_packets.size() == 3);
      }
    }
  }
}
