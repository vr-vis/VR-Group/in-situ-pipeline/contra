# ------------------------------------------------------------------------------
# contra -- a lightweight transport library for conduit data
#
# Copyright (c) 2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
# ------------------------------------------------------------------------------
#                                  License
#
# The license of the software changes depending on if it is compiled with or
# without ZeroMQ support. See the LICENSE file for more details.
# ------------------------------------------------------------------------------
#                          Apache License, Version 2.0
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------------
# Contra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Contra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Contra.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

find_package(Threads REQUIRED)

add_target(
  LIBRARY
  NAME websockets
  SOURCE_GLOB src/*.cpp
  HEADER_GLOB include/contra/websockets/*.hpp
  DEPENDENCIES
    PUBLIC contra::contra
    PUBLIC conduit
    PUBLIC Boost::boost
    PUBLIC Boost::system
    PUBLIC Boost::disable_autolinking
	PUBLIC Threads::Threads
  ${GLOBAL_TARGET_OPTIONS}
)

target_compile_definitions(websockets
  PUBLIC CONTRA_WEBSOCKETS_AVAILABLE=1
)

if (NOT MSVC)
  target_compile_options(websockets PRIVATE -fPIC)
endif (NOT MSVC)

if (ENABLE_TESTS)
  add_subdirectory(tests)
endif (ENABLE_TESTS)
