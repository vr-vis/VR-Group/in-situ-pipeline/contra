// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include "contra/websockets/bwebsockets_transport_server.hpp"

namespace contra {

BWSTServer::BWSTServer(uint16_t port) : port_(port), ioc_(1) {
  // Create and launch a listening port
  listener_thread_ = std::thread([this]() {
    auto const address = boost::asio::ip::make_address("127.0.0.1");
    listener_ = std::make_shared<Listener>(
        &ioc_, tcp::endpoint{address, port_}, [this](Packet& packet) -> void {
          {
            std::lock_guard<std::mutex> lock(mutex_);
            received_packets_.push_back(std::move(packet));
            notified_ = true;
          }
          packets_available_.notify_all();
        });
    listener_->Run();
  });
  std::this_thread::sleep_for(std::chrono::milliseconds(20));

  context_thread_ = std::thread([this] { ioc_.run(); });
}

void BWSTServer::Send(const Packet& packet,
                      bool cache_until_client_is_connected) {
  listener_->Send(packet, cache_until_client_is_connected);
}

std::vector<Packet> BWSTServer::Receive(int64_t timeout_ms) {
  std::unique_lock<std::mutex> lock(mutex_);

  if (timeout_ms == RECEIVE_WAIT_FOR_PACKET) {
    packets_available_.wait(lock, [this] { return notified_; });
  } else {
    packets_available_.wait_for(lock, std::chrono::milliseconds(timeout_ms),
                                [this] { return notified_; });
  }
  notified_ = false;

  std::vector<Packet> packets;
  packets.swap(received_packets_);
  return packets;
}

bool BWSTServer::IsConnected() const { return listener_->HasOpenSession(); }

BWSTServer::~BWSTServer() {
  ioc_.stop();
  context_thread_.join();
  listener_thread_.join();
  listener_.reset();
}

}  // namespace contra
