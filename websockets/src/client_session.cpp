// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include "contra/websockets/client_session.hpp"

namespace contra {

ClientSession::ClientSession(boost::asio::io_context* ioc,
                             std::function<void(Packet&)> receive_callback)
    : resolver_(*ioc), ws_(*ioc), receive_callback_(receive_callback) {}

void ClientSession::Run(char const* host, char const* port) {
  host_ = host;

  // Look up the domain name
  resolver_.async_resolve(
      host, port,
      std::bind(&ClientSession::OnResolve, shared_from_this(),
                std::placeholders::_1, std::placeholders::_2));
}

void ClientSession::OnResolve(boost::system::error_code ec,
                              tcp::resolver::results_type results) {
  if (ec) return Fail(ec, "resolve");

  // Make the connection on the IP address we get from a lookup
  boost::asio::async_connect(
      ws_.next_layer(), results.begin(), results.end(),
      std::bind(&ClientSession::OnConnect, shared_from_this(),
                std::placeholders::_1));
}

void ClientSession::OnConnect(boost::system::error_code ec) {
  if (ec) return Fail(ec, "connect");

  // Perform the websocket handshake
  ws_.async_handshake(host_, "/",
                      std::bind(&ClientSession::OnHandshake, shared_from_this(),
                                std::placeholders::_1));
}

void ClientSession::OnHandshake(boost::system::error_code ec) {
  if (ec) return Fail(ec, "handshake");

  connected_.store(true);

  ws_.async_read(receive_buffer_,
                 std::bind(&ClientSession::OnRead, shared_from_this(),
                           std::placeholders::_1, std::placeholders::_2));
}

void ClientSession::OnRead(boost::system::error_code ec,
                           std::size_t bytes_transferred) {
  boost::ignore_unused(bytes_transferred);
  if (ec) return Fail(ec, "read");

  auto str = boost::beast::buffers_to_string(receive_buffer_.data());
  auto packet =
      contra::DeserializePacket(std::vector<uint8_t>(str.begin(), str.end()));
  receive_callback_(packet);

  receive_buffer_.consume(receive_buffer_.size());

  ws_.async_read(receive_buffer_,
                 std::bind(&ClientSession::OnRead, shared_from_this(),
                           std::placeholders::_1, std::placeholders::_2));
}

bool ClientSession::IsConnected() { return connected_.load(); }

void ClientSession::Send(const Packet& packet) {
  while (busy_sending_) {
    std::this_thread::yield();
  }
  busy_sending_ = true;
  auto buffer = SerializePacket(packet);
  const auto str_buffer = std::string(buffer.begin(), buffer.end());

  size_t n = boost::asio::buffer_copy(send_buffer_.prepare(str_buffer.size()),
                                      boost::asio::buffer(str_buffer));
  send_buffer_.commit(n);

  ws_.async_write(send_buffer_.data(),
                  std::bind(&ClientSession::OnWrite, shared_from_this(),
                            std::placeholders::_1, std::placeholders::_2));
}

void ClientSession::OnWrite(boost::system::error_code ec,
                            std::size_t bytes_transferred) {
  boost::ignore_unused(bytes_transferred);

  if (ec) return Fail(ec, "write");

  // Clear the buffer
  send_buffer_.consume(send_buffer_.size());
  busy_sending_ = false;
}
void ClientSession::Close() {
  ws_.async_close(websocket::close_code::normal,
                  std::bind(&ClientSession::OnClose, shared_from_this(),
                            std::placeholders::_1));
}

void ClientSession::OnClose(boost::system::error_code ec) {
  if (ec) return Fail(ec, "close");
}

void ClientSession::Fail(boost::system::error_code ec, char const* what) {
  if (!std::string(ec.message())
           .compare("Der E/A-Vorgang wurde wegen eines Threadendes oder einer "
                    "Anwendungsanforderung abgebrochen")) {
    return;
  } else {
    std::cerr << "Client: " << what << ": " << ec.message() << "\n";
  }
}

}  // namespace contra
