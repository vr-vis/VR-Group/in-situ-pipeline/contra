// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include "contra/websockets/listener.hpp"

namespace contra {

Listener::Listener(boost::asio::io_context* ioc, tcp::endpoint endpoint,
                   std::function<void(Packet&)> receive_callback)
    : acceptor_(*ioc), socket_(*ioc), receive_callback_(receive_callback) {
  boost::system::error_code ec;

  // Open the acceptor
  acceptor_.open(endpoint.protocol(), ec);
  if (ec) {
    Fail(ec, "open");
    return;
  }

  // Allow address reuse
  acceptor_.set_option(boost::asio::socket_base::reuse_address(true), ec);
  if (ec) {
    Fail(ec, "set_option");
    return;
  }

  // Bind to the server address
  acceptor_.bind(endpoint, ec);
  if (ec) {
    Fail(ec, "bind");
    return;
  }

  // Start listening for connections
  acceptor_.listen(boost::asio::socket_base::max_listen_connections, ec);
  if (ec) {
    Fail(ec, "listen");
    return;
  }
}

void Listener::Run() {
  if (!acceptor_.is_open()) return;
  DoAccept();
}

void Listener::DoAccept() {
  acceptor_.async_accept(socket_,
                         std::bind(&Listener::OnAccept, shared_from_this(),
                                   std::placeholders::_1));
}

void Listener::OnAccept(boost::system::error_code ec) {
  if (ec) {
    Fail(ec, "accept");
  } else {
    std::lock_guard<std::mutex> lock(mutex_);
    sessions_.push_back(std::make_shared<ServerSession>(
        std::move(socket_), next_id_,
        [this](int id) -> void {
          for (auto it = sessions_.begin(); it != sessions_.end(); ++it) {
            if ((*it)->GetId() == id) {
              sessions_.erase(it);
              break;
            }
          }
        },
        receive_callback_));
    sessions_.back()->Run();
    next_id_++;
  }
  DoAccept();
}

bool Listener::HasOpenSession() {
  if (sessions_.empty()) {
    return false;
  }
  return true;
}

void Listener::Send(const Packet& packet,
                    bool buffer_message_until_client_connects) {
  (void)buffer_message_until_client_connects;
  std::lock_guard<std::mutex> lock(mutex_);
  for (auto& session : sessions_) {
    session->Send(packet);
  }
}

void Listener::Fail(boost::system::error_code ec, char const* what) {
  std::cerr << "Listener: " << what << ": " << ec.message() << "\n";
}

}  // namespace contra
