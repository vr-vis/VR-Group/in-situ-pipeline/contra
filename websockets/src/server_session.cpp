// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include "contra/websockets/server_session.hpp"

namespace contra {

ServerSession::ServerSession(tcp::socket socket, int id,
                             std::function<void(int)> delete_callback,
                             std::function<void(Packet&)> receive_callback)
    : ws_(std::move(socket)),
      strand_(ws_.get_executor()),
      session_id_(id),
      delete_callback_(delete_callback),
      receive_callback_(receive_callback) {
  ws_.binary(true);
}

void ServerSession::Run() {
  ws_.async_accept(boost::asio::bind_executor(
      strand_, std::bind(&ServerSession::OnAccept, shared_from_this(),
                         std::placeholders::_1)));
}

void ServerSession::OnAccept(boost::system::error_code ec) {
  if (ec) return Fail(ec, "accept");
  ready_ = true;
  DoRead();
}

void ServerSession::Send(const Packet& packet) {
  if (!ready_) {
    return;
  }
  while (busy_sending_) {
    std::this_thread::yield();
  }
  busy_sending_ = true;
  auto buffer = SerializePacket(packet);
  const auto str_buffer = std::string(buffer.begin(), buffer.end());

  size_t n = boost::asio::buffer_copy(send_buffer_.prepare(str_buffer.size()),
                                      boost::asio::buffer(str_buffer));
  send_buffer_.commit(n);

  ws_.async_write(
      send_buffer_.data(),
      boost::asio::bind_executor(
          strand_, std::bind(&ServerSession::OnWrite, shared_from_this(),
                             std::placeholders::_1, std::placeholders::_2)));
}

void ServerSession::OnWrite(boost::system::error_code ec,
                            std::size_t bytes_transferred) {
  boost::ignore_unused(bytes_transferred);

  if (ec) return Fail(ec, "write");

  // Clear the buffer
  send_buffer_.consume(send_buffer_.size());
  busy_sending_ = false;
}

void ServerSession::DoRead() {
  // Read a message into our buffer
  ws_.async_read(
      receive_buffer_,
      boost::asio::bind_executor(
          strand_, std::bind(&ServerSession::OnRead, shared_from_this(),
                             std::placeholders::_1, std::placeholders::_2)));
}

void ServerSession::OnRead(boost::system::error_code ec,
                           std::size_t bytes_transferred) {
  boost::ignore_unused(bytes_transferred);

  if (ec == websocket::error::closed) {
    // This indicates that the session was closed
    delete_callback_(session_id_);
    return;
  } else {
    if (ec) return Fail(ec, "read");

    auto str = boost::beast::buffers_to_string(receive_buffer_.data());
    auto packet =
        contra::DeserializePacket(std::vector<uint8_t>(str.begin(), str.end()));
    receive_callback_(packet);

    receive_buffer_.consume(receive_buffer_.size());
    ws_.async_read(receive_buffer_,
                   std::bind(&ServerSession::OnRead, shared_from_this(),
                             std::placeholders::_1, std::placeholders::_2));
  }
}

int ServerSession::GetId() const { return session_id_; }

void ServerSession::Fail(boost::system::error_code ec, char const* what) {
  std::cerr << "Server: " << what << ": " << ec.message() << "\n";
}

}  // namespace contra
