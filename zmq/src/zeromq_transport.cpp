// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include "contra/zmq/zeromq_transport.hpp"

#include <iostream>
#include <string>
#include <vector>

#include "zmq.hpp"

namespace contra {

ZMQTransport::ZMQTransport(const std::string& address)
    : context_(1), socket_(context_, ZMQ_DEALER) {
  monitor_.init(socket_, "inproc://Client-Monitor");
  socket_.connect("tcp://" + address);
  running_.store(true);
  monitor_thread_ = std::thread([this] {
    while (running_.load()) {
      monitor_.check_event();
      std::this_thread::yield();
    }
  });
}

ZMQTransport::ZMQTransport(uint16_t port)
    : context_(1), socket_(context_, ZMQ_DEALER) {
  monitor_.init(socket_, "inproc://Server-Monitor");
  socket_.bind("tcp://*:" + std::to_string(port));
  running_.store(true);
  monitor_thread_ = std::thread([this] {
    while (running_.load()) {
      monitor_.check_event();
      std::this_thread::yield();
    }
  });
}

void ZMQTransport::Send(const Packet& packet,
                        bool cache_until_client_is_connected) {
  auto buffer = SerializePacket(packet);
  if (!socket_.send(buffer.data(), buffer.size(),
                    cache_until_client_is_connected ? 0 : ZMQ_DONTWAIT)) {
    std::cout << "WARNING: No client available! Data is Lost!" << std::endl;
  }
}

std::vector<Packet> ZMQTransport::Receive(int64_t timeout_ms) {
  std::vector<Packet> packets;

  zmq::message_t received_message;
  std::vector<uint8_t> message;

  // Receive all messages, but only block for the first one
  socket_.setsockopt(ZMQ_RCVTIMEO, static_cast<int>(timeout_ms));
  while (
      socket_.recv(&received_message, packets.size() > 0 ? ZMQ_DONTWAIT : 0)) {
    message.clear();
    message.resize(received_message.size());  // #95
    std::memcpy(message.data(), received_message.data(),
                received_message.size());           // #95
    packets.push_back(DeserializePacket(message));  // #95
  }

  return packets;
}

bool ZMQTransport::IsConnected() const {
  return monitor_.GetConnections() > 0 ? true : false;
}

ZMQTransport::~ZMQTransport() {
  running_.store(false);
  monitor_thread_.join();
}

}  // namespace contra
