// -----------------------------------------------------------------------------
// contra -- a lightweight transport library for conduit data
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// The license of the software changes depending on if it is compiled with or
// without ZeroMQ support. See the LICENSE file for more details.
// -----------------------------------------------------------------------------
//                          Apache License, Version 2.0
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------
// Contra is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Contra is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Contra.  If not, see <https://www.gnu.org/licenses/>.
// -----------------------------------------------------------------------------

#include <cstring>
#include <iostream>
#include <string>
#include <thread>

#include "contra/relay.hpp"

#if CONTRA_SHARED_MEMORY_AVAILABLE
#include "contra/shared_memory/shared_memory_transport.hpp"
#endif  // CONTRA_SHARED_MEMORY_AVAILABLE

#if CONTRA_WEBSOCKETS_AVAILABLE
#include "contra/websockets/bwebsockets_transport_client.hpp"
#endif  // CONTRA_WEBSOCKETS_AVAILABLE

#if CONTRA_ZMQ_AVAILABLE
#include "contra/zmq/zeromq_transport.hpp"
#endif  // CONTRA_ZMQ_AVAILABLE

void InvalidInput() {
  std::cout << "Invalid input use the following:\n[transport_type] "
               "[constructor_argument_1] [constructor_argument_2] [...]\n"
               "Possible ways to use this are:\n"
#if CONTRA_WEBSOCKETS_AVAILABLE
               "websockets <address:port> <timeout_in_ms> \n"
#endif  // CONTRA_WEBSOCKETS_AVAILABLE
#if CONTRA_SHARED_MEMORY_AVAILABLE
               "shared_memory <memory_name>\n"
#endif  // CONTRA_SHARED_MEMORY_AVAILABLE
#if CONTRA_ZMQ_AVAILABLE
               "zmq_server <(tcp://)address:port>\n"
               "zmq_client <(tcp://)address:port>\n"
#endif  // CONTRA_ZMQ_AVAILABLE
            << std::endl;
}

template <typename Relay>
[[noreturn]] void Run(Relay* relay) {
  std::vector<conduit::Node> data;

  while (true) {
    data = relay->Receive(100);
    for (auto& node : data) {
      node.print();
    }
  }
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    InvalidInput();
  }
#if CONTRA_WEBSOCKETS_AVAILABLE
  else if (strcmp(argv[1], "websockets") == 0 && argc == 4) {
    std::string address = argv[2];
    int64_t timeout = std::atoi(argv[3]);

    contra::Relay<contra::BWSTClient> relay(address, timeout);
    Run(&relay);
  }
#endif  // CONTRA_WEBSOCKETS_AVAILABLE
#if CONTRA_SHARED_MEMORY_AVAILABLE
  else if (strcmp(argv[1], "shared_memory") == 0) {
    // TODO() Shared memory doesn't seem to be working the way its intended to
    // do.
    std::string name = "contraShMemTransp";
    if (argc == 3) {
      name = argv[2];
    }
    contra::Relay<contra::SharedMemoryTransport> relay(name);
    Run(&relay);
  }
#endif  // CONTRA_SHARED_MEMORY_AVAILABLE
#if CONTRA_ZMQ_AVAILABLE
  else if (strcmp(argv[1], "zmq_server") == 0) {
    uint16_t port = 5555;
    if (argc == 3) {
      port = std::atoi(argv[2]);
    }
    contra::Relay<contra::ZMQTransport> relay(port);
    Run(&relay);
  } else if (strcmp(argv[1], "zmq_client") == 0) {
    std::string address = "localhost:5555";
    if (argc == 3) {
      address = argv[2];
    }
    contra::Relay<contra::ZMQTransport> relay(address);
    Run(&relay);
  }
#endif  // CONTRA_ZMQ_AVAILABLE
  else {
    InvalidInput();
  }

  return EXIT_SUCCESS;
}
