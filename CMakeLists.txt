# ------------------------------------------------------------------------------
# contra -- a lightweight transport library for conduit data
#
# Copyright (c) 2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
# ------------------------------------------------------------------------------
#                                  License
#
# The license of the software changes depending on if it is compiled with or
# without ZeroMQ support. See the LICENSE file for more details.
# ------------------------------------------------------------------------------
#                          Apache License, Version 2.0
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------------
# Contra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Contra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Contra.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.7.0)

project(contra)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake-utilities)
set(CMAKE_CXX_STANDARD 14)

if(UNIX AND NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

include(AddTarget)
find_package(Conduit REQUIRED)

option(ENABLE_TESTS "Enable testing to ensure contra is working correctly" ON)
option(ENABLE_DEVELOPER_TESTS "Enable cppcheck and cpplint tests, only interesting for developers" OFF)
option(WITH_ZEROMQ "Build with ZeroMQ transport layer" OFF)
option(WITH_SHARED_MEMORY "Build with boost shared memory transport layer" OFF)
option(WITH_PYTHON_BINDINGS "Create python bindings for the library" OFF)
option(BUILD_BENCHMARK "Build benchmark tool" OFF)
option(WITH_WEBSOCKETS "Build with boost Websocket transport layer" OFF)
option(WITH_HELPER_APPS "Build additional helper apps for testing purposes" OFF)

if ( NOT WITH_ZEROMQ AND NOT WITH_SHARED_MEMORY AND NOT WITH_WEBSOCKETS)
  message("WARNING: You are only building the Core lib of Contra. Make sure to build with at least one Transport Layer if you want to use Contra")
endif (NOT WITH_ZEROMQ AND NOT WITH_SHARED_MEMORY AND NOT WITH_WEBSOCKETS)

if (NOT ENABLE_TESTS)
  message(STATUS "ENABLE_TESTS was set to `OFF` cmake will skip the creation of all tests")
endif (NOT ENABLE_TESTS)

if (ENABLE_TESTS AND NOT ENABLE_DEVELOPER_TESTS)
  message(STATUS "ENABLE_TESTS was set to `ON` you can run the tests after building the library to ensure it is working correctly.")
endif (ENABLE_TESTS AND NOT ENABLE_DEVELOPER_TESTS)

if (NOT ENABLE_TESTS AND ENABLE_DEVELOPER_TESTS)
  message(SEND_ERROR "ENABLE_DEVELOPER_TESTS was set to `ON` but tests. To enable the developer tests you also need to set ENABLE_TESTS to `ON`.")
endif (NOT ENABLE_TESTS AND ENABLE_DEVELOPER_TESTS)

if (NOT ENABLE_DEVELOPER_TESTS)
  set(GLOBAL_TARGET_OPTIONS NO_TESTS)
endif ()

if (ENABLE_TESTS)
  enable_testing()
  include(CTest)
  add_subdirectory(test_utilities)
endif (ENABLE_TESTS)

if (WITH_ZEROMQ)
  find_package(cppzmq REQUIRED)
  add_definitions(-DWITH_ZEROMQ=1)
  add_subdirectory(zmq)
endif()

if (WITH_SHARED_MEMORY)
  find_package(Boost REQUIRED)
  add_definitions(-DWITH_SHARED_MEMORY=1)
  add_subdirectory(shared_memory)
endif()

if (WITH_WEBSOCKETS)
  find_package(Boost REQUIRED system)
  add_definitions(-DWITH_WEBSOCKETS=1)
  add_subdirectory(websockets)
endif()

if (WITH_HELPER_APPS)
  add_subdirectory(helper_apps)
endif()

add_subdirectory(contra)

if (BUILD_BENCHMARK)
  add_subdirectory(benchmark)
endif (BUILD_BENCHMARK)

if (WITH_PYTHON_BINDINGS)
  include(python_module)

  if (ENABLE_TESTS)
    include(py.test)
  endif (ENABLE_TESTS)

  find_package(PythonInterp REQUIRED)
  find_package(PythonLibs ${PYTHON_VERSION_STRING} EXACT REQUIRED)
  find_package(Boost REQUIRED COMPONENTS python${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR})

  add_subdirectory(pycontra)
endif (WITH_PYTHON_BINDINGS)

install(
  FILES ${CMAKE_SOURCE_DIR}/cmake/contra-config.cmake
        ${CMAKE_SOURCE_DIR}/cmake/FindConduit.cmake
  DESTINATION lib/cmake/contra)
