# ------------------------------------------------------------------------------
# contra -- a lightweight transport library for conduit data
#
# Copyright (c) 2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
# ------------------------------------------------------------------------------
#                                  License
#
# The license of the software changes depending on if it is compiled with or
# without ZeroMQ support. See the LICENSE file for more details.
# ------------------------------------------------------------------------------
#                          Apache License, Version 2.0
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------------
# Contra is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Contra is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Contra.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

find_file(
  Conduit_CONFIG_FILE
  "conduit.cmake"
  HINTS
    ${Conduit_DIR}
    ${CONDUIT_DIR}/lib/cmake  # For backwards comability
  PATH_SUFFIXES
    lib/cmake
)

find_path(
  Conduit_INCLUDE_DIRECTORIES
  "conduit/conduit.h"
  HINTS
    ${Conduit_DIR}/../../include
    ${CONDUIT_DIR}/include  # For backwards comability
)

mark_as_advanced(
  Conduit_CONFIG_FILE
  Conduit_INCLUDE_DIRECTORIES
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Conduit
  FOUND_VAR Conduit_FOUND
  REQUIRED_VARS
    Conduit_CONFIG_FILE
    Conduit_INCLUDE_DIRECTORIES
)

if (Conduit_FOUND AND NOT TARGET conduit)
  include(${Conduit_CONFIG_FILE})
  set_target_properties(conduit
    PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES ${Conduit_INCLUDE_DIRECTORIES}
  )
endif ()
